//
//  CategoryView.swift
//  iOSTest
//
//  Created by khaledannajar on 3/3/16.
//  Copyright © 2016 smarttech. All rights reserved.
//

import UIKit

class CategoryViewController : UICollectionViewController {
    private let reuseIdentifier = "categoryBookCell"
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var dataController : DataController {
        return self.appDelegate.dataController!
    }
    
    let refreshCtrl = UIRefreshControl()
    
    var dataSource: [AnyObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSLog("view did load");
        
        self.dataSource = dataController.fetchAllBooks()
        self.collectionView?.reloadData()
        
        refreshCtrl.addTarget(self, action: "callService", forControlEvents: .ValueChanged)
        self.collectionView?.addSubview(refreshCtrl)
        self.collectionView?.alwaysBounceVertical = true
        
       self.callService()
        
    }
    
    func callService() {
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        NetworkCaller.instance.getCategoryBooks(683, limit: 2,
            success: { (category:Category!) -> () in
                
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.refreshCtrl.endRefreshing()
                NSLog("result = %@", category.books!)
                
                if let books = category.books {
                    self.dataSource = books;
                    self.collectionView?.reloadData()
                    self.dataController .saveBooks(books)
                }
                
            }, failure: { error in
                
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.refreshCtrl.endRefreshing()
                NSLog("error =%@", error)
        });
    }
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
        
    {
        return self.dataSource.count;
    }
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! BookCollectionCell
        
        let book = self.dataSource[indexPath.row] as! Book
        
        cell.bookTitleLabel.text = book.name
        
        cell.bookImageView.imageFromUrl(book.image!, placeHolderImageName: "book_default")
        
        return cell;
    }
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1;
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let indexPath = self.collectionView?.indexPathsForSelectedItems()?.first
        let book = self.dataSource[(indexPath?.row)!]
        let bookDetailsViewController = segue.destinationViewController as! BookDetailsViewController
        bookDetailsViewController.book = book as? Book
        
    }
}

extension UIImageView {
    public func imageFromUrl(url: String, placeHolderImageName: String) {
        
        self.contentMode = UIViewContentMode.ScaleAspectFill
        self.image = UIImage(named: placeHolderImageName)
        
        NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: url)!) { (data, response, error) in
            dispatch_async(dispatch_get_main_queue()) {
                self.contentMode = UIViewContentMode.ScaleAspectFill
                self.image = UIImage(data: data!)
            }
            }.resume()
    }
}
