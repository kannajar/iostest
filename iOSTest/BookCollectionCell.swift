//
//  BookCollectionCell.swift
//  iOSTest
//
//  Created by khaledannajar on 3/3/16.
//  Copyright © 2016 smarttech. All rights reserved.
//

import UIKit

class BookCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var bookImageView: UIImageView!
    
    @IBOutlet weak var bookTitleLabel: UILabel!
}
