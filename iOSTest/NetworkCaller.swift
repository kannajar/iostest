//
//  NetworkCaller.swift
//  iOSTest
//
//  Created by khaledannajar on 3/3/16.
//  Copyright © 2016 smarttech. All rights reserved.
//

import Foundation
import Alamofire

class NetworkCaller {
    
    static let instance = NetworkCaller()
    
    enum SubUrls : String {
        case books = "book.json"
        case other = "other"
    }
    
    let baseUrl = "http://www.aqeedeh.com/webservice/" as String
    // http://www.aqeedeh.com/webservice/book.json?id=683&limit=1
    
    
    func getCategoryBooks (categoryId : Int, limit : Int, success successBlock : (Category! -> ())!,
        failure failureBlock : (NSError! -> ())!)
    {
        let url = baseUrl + SubUrls.books.rawValue;
        
        Alamofire.request(.GET, url, parameters: ["id": categoryId, "limit" : limit]).responseJSON { response in

            if let JSON = response.result.value {

                let categoryx = Category(object: JSON)
                print(categoryx)
                
                successBlock(categoryx)
            } else if let error = response.result.error
            {
                failureBlock(error)
            } else {
                let userInfo: [NSObject : AnyObject] =
                [
                    NSLocalizedDescriptionKey :  NSLocalizedString("Error", value: "Error", comment: ""),
                    NSLocalizedFailureReasonErrorKey : NSLocalizedString("Error", value: "Error", comment: "")
                ]
                failureBlock(NSError(domain: "Network", code: 1500, userInfo: userInfo))
            }
        }
    }
}
