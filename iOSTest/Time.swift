//
//  Time.swift
//
//  Created by khaledannajar on 3/3/16
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class Time: NSObject, NSCoding {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kTimeQueryBuildKey: String = "query_build"
	internal let kTimeQueryTimeKey: String = "queryTime"
	internal let kTimeBuildOutputKey: String = "build_output"
	internal let kTimeIncludeKey: String = "include"


    // MARK: Properties
	public var queryBuild: Float?
	public var queryTime: Float?
	public var buildOutput: Float?
	public var include: Float?


    // MARK: SwiftyJSON Initalizers
    /**
    Initates the class based on the object
    - parameter object: The object of either Dictionary or Array kind that was passed.
    - returns: An initalized instance of the class.
    */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }

    /**
    Initates the class based on the JSON that was passed.
    - parameter json: JSON object from SwiftyJSON.
    - returns: An initalized instance of the class.
    */
    public init(json: JSON) {
		queryBuild = json[kTimeQueryBuildKey].float
		queryTime = json[kTimeQueryTimeKey].float
		buildOutput = json[kTimeBuildOutputKey].float
		include = json[kTimeIncludeKey].float

    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if queryBuild != nil {
			dictionary.updateValue(queryBuild!, forKey: kTimeQueryBuildKey)
		}
		if queryTime != nil {
			dictionary.updateValue(queryTime!, forKey: kTimeQueryTimeKey)
		}
		if buildOutput != nil {
			dictionary.updateValue(buildOutput!, forKey: kTimeBuildOutputKey)
		}
		if include != nil {
			dictionary.updateValue(include!, forKey: kTimeIncludeKey)
		}

        return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
		self.queryBuild = aDecoder.decodeObjectForKey(kTimeQueryBuildKey) as? Float
		self.queryTime = aDecoder.decodeObjectForKey(kTimeQueryTimeKey) as? Float
		self.buildOutput = aDecoder.decodeObjectForKey(kTimeBuildOutputKey) as? Float
		self.include = aDecoder.decodeObjectForKey(kTimeIncludeKey) as? Float

    }

    public func encodeWithCoder(aCoder: NSCoder) {
		aCoder.encodeObject(queryBuild, forKey: kTimeQueryBuildKey)
		aCoder.encodeObject(queryTime, forKey: kTimeQueryTimeKey)
		aCoder.encodeObject(buildOutput, forKey: kTimeBuildOutputKey)
		aCoder.encodeObject(include, forKey: kTimeIncludeKey)

    }

}
