//
//  DataManager.swift
//  iOSTest
//
//  Created by khaledannajar on 3/8/16.
//  Copyright © 2016 smarttech. All rights reserved.
//

import Foundation
import CoreData

class DataController: NSObject {
    static let instance = DataController()
    
    let idAttribute = "identifier"
    
    var managedObjectContext: NSManagedObjectContext
    
    private override init() {
        // This resource is the same name as your xcdatamodeld contained in your project.
        guard let modelURL = NSBundle.mainBundle().URLForResource("iOSTest", withExtension:"momd") else {
            fatalError("Error loading model from bundle")
        }
        // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
        guard let mom = NSManagedObjectModel(contentsOfURL: modelURL) else {
            fatalError("Error initializing mom from: \(modelURL)")
        }
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        self.managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        self.managedObjectContext.persistentStoreCoordinator = psc
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
            let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
            let docURL = urls[urls.endIndex-1]
            /* The directory the application uses to store the Core Data store file.
            This code uses a file named "DataModel.sqlite" in the application's documents directory.
            */
            let storeURL = docURL.URLByAppendingPathComponent("iOSTest.sqlite")
            do {
                try psc.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeURL, options: nil)
            } catch {
                fatalError("Error migrating store: \(error)")
            }
        }
    }
    
    func saveBooks(books : [Book]) {
        for book in books {
            
            if !self.isSavedAlreadyBookEntityWithId(book.internalIdentifier!){
                createBookfromBook(book)
            }
        }
        
        do {
            try managedObjectContext.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    
    func createBookfromBook(book : Book) -> BookEntity {
        
        let entityDescription = NSEntityDescription.entityForName(NSStringFromClass(BookEntity), inManagedObjectContext: managedObjectContext)
        let entity = NSManagedObject(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext) as! BookEntity
        entity.name = book.name
        entity.image = book.image
        entity.minbody = book.minbody
        entity.identifier = book.internalIdentifier
        return entity;
        
    }
    
    func isSavedAlreadyBookEntityWithId(bookId : String) -> Bool
    {
        let fetchRequest :NSFetchRequest = NSFetchRequest(entityName: NSStringFromClass(BookEntity))
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate = NSPredicate(format: "%K == %@", idAttribute, bookId)

        var error: NSError? = nil
        let count = managedObjectContext.countForFetchRequest(fetchRequest, error: &error )
        
        let exists = count > 0
        return exists;
    }
    
    func fetchBookEntityWithId(bookId : String) -> BookEntity
    {
        let fetchRequest :NSFetchRequest = NSFetchRequest(entityName: NSStringFromClass(BookEntity))
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate = NSPredicate(format: "%K == %@", idAttribute, bookId)

        do {
            let fetchedBooks = try managedObjectContext.executeFetchRequest(fetchRequest) as! [BookEntity]
            return fetchedBooks.first!
        } catch {
            fatalError("Failed to fetch employees: \(error)")
        }
    }
    
    func fetchBookEntities() -> [BookEntity]
    {
        let fetchRequest :NSFetchRequest = NSFetchRequest(entityName: NSStringFromClass(BookEntity))
        
        do {
            let fetchedBooks = try managedObjectContext.executeFetchRequest(fetchRequest) as! [BookEntity]
            return fetchedBooks
        } catch {
            fatalError("Failed to fetch employees: \(error)")
        }
    }
    
    func fetchAllBooks() -> [Book] {
        var result = [Book]()
        let bookEntities = fetchBookEntities()
        for bookEntity in bookEntities {
            let book :Book = Book()
            book.name = bookEntity.name
            book.internalIdentifier = bookEntity.identifier
            book.image = bookEntity.image
            book.minbody = bookEntity.minbody
            result.append(book)
        }
        return result
    }
}
