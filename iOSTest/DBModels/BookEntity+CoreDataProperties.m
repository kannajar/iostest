//
//  BookEntity+CoreDataProperties.m
//  
//
//  Created by khaledannajar on 3/8/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BookEntity+CoreDataProperties.h"

@implementation BookEntity (CoreDataProperties)

@dynamic identifier;
@dynamic name;
@dynamic minbody;
@dynamic image;

@end
