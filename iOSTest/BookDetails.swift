//
//  BookDetails.swift
//  iOSTest
//
//  Created by khaledannajar on 3/3/16.
//  Copyright © 2016 smarttech. All rights reserved.
//

import UIKit

class BookDetailsViewController : UIViewController {
    
    var book : Book?
    
    @IBOutlet weak var detialsTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupNavigationItem()
     
        let minBodyData = self.book?.minbody?.dataUsingEncoding(NSUTF8StringEncoding)!
        let attributedOptions : [String: AnyObject] = [
            NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
            NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding
        ]
        
        do {
            let attributedString = try NSAttributedString(data: minBodyData!, options: attributedOptions, documentAttributes: nil)
        
            self.detialsTextView.attributedText = attributedString
        } catch {
            print(error)
        }
    }
    
    func setupNavigationItem() {
        
        self.navigationItem.title = self.book?.name
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        let backColor = UIColor(red: 198.0/255.0, green: 147.0/255.0, blue: 0, alpha: 1)
        
        let image =        imageForColor(backColor, forSize:CGSizeMake(70, 30), withCornerRadius: 4)
        
        let backCustomButton = UIButton()
        
        backCustomButton.setBackgroundImage(image, forState: .Normal)
        backCustomButton.setTitle("رجوع", forState: .Normal)
        backCustomButton.frame = CGRectMake(0, 0, 60, 30)
        backCustomButton.addTarget(self, action: Selector("backTapped"), forControlEvents: .TouchUpInside)
        backCustomButton.layer.cornerRadius = 4;
        
        let backBarButton = UIBarButtonItem()
        backBarButton.customView = backCustomButton
        self.navigationItem.leftBarButtonItem = backBarButton
    }
    
    func backTapped() {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
