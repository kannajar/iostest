//
//  Utils.swift
//  iOSTest
//
//  Created by khaledannajar on 3/14/16.
//  Copyright © 2016 smarttech. All rights reserved.
//

import UIKit

func imageForColor(color: UIColor , forSize size : CGSize, withCornerRadius radius: CGFloat) -> UIImage
{
    let rect = CGRectMake(0, 0, size.width, size.height)
    UIGraphicsBeginImageContext(rect.size);
    
    let context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    
    var image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Begin a new image that will be the new image with the rounded corners
    // (here with the size of an UIImageView)
    UIGraphicsBeginImageContext(size);
    
    // Add a clip before drawing anything, in the shape of an rounded rect
    let bath = UIBezierPath(roundedRect:rect, cornerRadius:radius);
    bath.addClip();
    
    // Draw your image
    image.drawInRect(rect)

    
    // Get the image, here setting the UIImageView image
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Lets forget about that we were drawing
    UIGraphicsEndImageContext();

    return image;
}
