//
//  Book.swift
//
//  Created by khaledannajar on 3/3/16
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class Book: NSObject, NSCoding {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kBookCategoriesKey: String = "categories"
	internal let kBookActiveKey: String = "active"
	internal let kBookParentKey: String = "parent"
	internal let kBookIsActiveKey: String = "isActive"
	internal let kBookPdfSizeKey: String = "pdf_size"
	internal let kBookNameKey: String = "name"
	internal let kBookLangKey: String = "lang"
	internal let kBookFlagKey: String = "flag"
	internal let kBookImageOriginalKey: String = "image_original"
	internal let kBookInternalIdentifierKey: String = "id"
	internal let kBookTransIdKey: String = "trans_id"
	internal let kBookMinbodyKey: String = "minbody"
	internal let kBookPdfPureFileKey: String = "pdf_pure_file"
	internal let kBookPagesKey: String = "pages"
	internal let kBookVisitKey: String = "visit"
	internal let kBookSubject2Key: String = "subject2"
	internal let kBookPdfFileKey: String = "pdf_file"
	internal let kBookWriteridKey: String = "writerid"
	internal let kBookLoadKey: String = "load"
	internal let kBookContentKey: String = "content"
	internal let kBookConsiderationKey: String = "consideration"
	internal let kBookAddDateKey: String = "add_date"
	internal let kBookImageKey: String = "image"
	internal let kBookTextDateKey: String = "text_date"
	internal let kBookHtmlSizeKey: String = "html_size"
	internal let kBookKeywordsKey: String = "keywords"
	internal let kBookReadKey: String = "read"
	internal let kBookSubjectKey: String = "subject"
	internal let kBookSubject3Key: String = "subject3"
	internal let kBookUpDateKey: String = "up_date"


    // MARK: Properties
	public var categories: String?
	public var active: String?
	public var parent: String?
	public var isActive: String?
	public var pdfSize: String?
	public var name: String?
	public var lang: String?
	public var flag: String?
	public var imageOriginal: String?
	public var internalIdentifier: String?
	public var transId: String?
	public var minbody: String?
	public var pdfPureFile: String?
	public var pages: String?
	public var visit: String?
	public var subject2: String?
	public var pdfFile: String?
	public var writerid: String?
	public var load: String?
	public var content: String?
	public var consideration: String?
	public var addDate: String?
	public var image: String?
	public var textDate: String?
	public var htmlSize: String?
	public var keywords: String?
	public var read: String?
	public var subject: String?
	public var subject3: String?
	public var upDate: String?


    // MARK: SwiftyJSON Initalizers
    /**
    Initates the class based on the object
    - parameter object: The object of either Dictionary or Array kind that was passed.
    - returns: An initalized instance of the class.
    */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }
    public override init() {
        
    }
    /**
    Initates the class based on the JSON that was passed.
    - parameter json: JSON object from SwiftyJSON.
    - returns: An initalized instance of the class.
    */
    public init(json: JSON) {
		categories = json[kBookCategoriesKey].string
		active = json[kBookActiveKey].string
		parent = json[kBookParentKey].string
		isActive = json[kBookIsActiveKey].string
		pdfSize = json[kBookPdfSizeKey].string
		name = json[kBookNameKey].string
		lang = json[kBookLangKey].string
		flag = json[kBookFlagKey].string
		imageOriginal = json[kBookImageOriginalKey].string
		internalIdentifier = json[kBookInternalIdentifierKey].string
		transId = json[kBookTransIdKey].string
		minbody = json[kBookMinbodyKey].string
		pdfPureFile = json[kBookPdfPureFileKey].string
		pages = json[kBookPagesKey].string
		visit = json[kBookVisitKey].string
		subject2 = json[kBookSubject2Key].string
		pdfFile = json[kBookPdfFileKey].string
		writerid = json[kBookWriteridKey].string
		load = json[kBookLoadKey].string
		content = json[kBookContentKey].string
		consideration = json[kBookConsiderationKey].string
		addDate = json[kBookAddDateKey].string
		image = json[kBookImageKey].string
		textDate = json[kBookTextDateKey].string
		htmlSize = json[kBookHtmlSizeKey].string
		keywords = json[kBookKeywordsKey].string
		read = json[kBookReadKey].string
		subject = json[kBookSubjectKey].string
		subject3 = json[kBookSubject3Key].string
		upDate = json[kBookUpDateKey].string

    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if categories != nil {
			dictionary.updateValue(categories!, forKey: kBookCategoriesKey)
		}
		if active != nil {
			dictionary.updateValue(active!, forKey: kBookActiveKey)
		}
		if parent != nil {
			dictionary.updateValue(parent!, forKey: kBookParentKey)
		}
		if isActive != nil {
			dictionary.updateValue(isActive!, forKey: kBookIsActiveKey)
		}
		if pdfSize != nil {
			dictionary.updateValue(pdfSize!, forKey: kBookPdfSizeKey)
		}
		if name != nil {
			dictionary.updateValue(name!, forKey: kBookNameKey)
		}
		if lang != nil {
			dictionary.updateValue(lang!, forKey: kBookLangKey)
		}
		if flag != nil {
			dictionary.updateValue(flag!, forKey: kBookFlagKey)
		}
		if imageOriginal != nil {
			dictionary.updateValue(imageOriginal!, forKey: kBookImageOriginalKey)
		}
		if internalIdentifier != nil {
			dictionary.updateValue(internalIdentifier!, forKey: kBookInternalIdentifierKey)
		}
		if transId != nil {
			dictionary.updateValue(transId!, forKey: kBookTransIdKey)
		}
		if minbody != nil {
			dictionary.updateValue(minbody!, forKey: kBookMinbodyKey)
		}
		if pdfPureFile != nil {
			dictionary.updateValue(pdfPureFile!, forKey: kBookPdfPureFileKey)
		}
		if pages != nil {
			dictionary.updateValue(pages!, forKey: kBookPagesKey)
		}
		if visit != nil {
			dictionary.updateValue(visit!, forKey: kBookVisitKey)
		}
		if subject2 != nil {
			dictionary.updateValue(subject2!, forKey: kBookSubject2Key)
		}
		if pdfFile != nil {
			dictionary.updateValue(pdfFile!, forKey: kBookPdfFileKey)
		}
		if writerid != nil {
			dictionary.updateValue(writerid!, forKey: kBookWriteridKey)
		}
		if load != nil {
			dictionary.updateValue(load!, forKey: kBookLoadKey)
		}
		if content != nil {
			dictionary.updateValue(content!, forKey: kBookContentKey)
		}
		if consideration != nil {
			dictionary.updateValue(consideration!, forKey: kBookConsiderationKey)
		}
		if addDate != nil {
			dictionary.updateValue(addDate!, forKey: kBookAddDateKey)
		}
		if image != nil {
			dictionary.updateValue(image!, forKey: kBookImageKey)
		}
		if textDate != nil {
			dictionary.updateValue(textDate!, forKey: kBookTextDateKey)
		}
		if htmlSize != nil {
			dictionary.updateValue(htmlSize!, forKey: kBookHtmlSizeKey)
		}
		if keywords != nil {
			dictionary.updateValue(keywords!, forKey: kBookKeywordsKey)
		}
		if read != nil {
			dictionary.updateValue(read!, forKey: kBookReadKey)
		}
		if subject != nil {
			dictionary.updateValue(subject!, forKey: kBookSubjectKey)
		}
		if subject3 != nil {
			dictionary.updateValue(subject3!, forKey: kBookSubject3Key)
		}
		if upDate != nil {
			dictionary.updateValue(upDate!, forKey: kBookUpDateKey)
		}

        return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
		self.categories = aDecoder.decodeObjectForKey(kBookCategoriesKey) as? String
		self.active = aDecoder.decodeObjectForKey(kBookActiveKey) as? String
		self.parent = aDecoder.decodeObjectForKey(kBookParentKey) as? String
		self.isActive = aDecoder.decodeObjectForKey(kBookIsActiveKey) as? String
		self.pdfSize = aDecoder.decodeObjectForKey(kBookPdfSizeKey) as? String
		self.name = aDecoder.decodeObjectForKey(kBookNameKey) as? String
		self.lang = aDecoder.decodeObjectForKey(kBookLangKey) as? String
		self.flag = aDecoder.decodeObjectForKey(kBookFlagKey) as? String
		self.imageOriginal = aDecoder.decodeObjectForKey(kBookImageOriginalKey) as? String
		self.internalIdentifier = aDecoder.decodeObjectForKey(kBookInternalIdentifierKey) as? String
		self.transId = aDecoder.decodeObjectForKey(kBookTransIdKey) as? String
		self.minbody = aDecoder.decodeObjectForKey(kBookMinbodyKey) as? String
		self.pdfPureFile = aDecoder.decodeObjectForKey(kBookPdfPureFileKey) as? String
		self.pages = aDecoder.decodeObjectForKey(kBookPagesKey) as? String
		self.visit = aDecoder.decodeObjectForKey(kBookVisitKey) as? String
		self.subject2 = aDecoder.decodeObjectForKey(kBookSubject2Key) as? String
		self.pdfFile = aDecoder.decodeObjectForKey(kBookPdfFileKey) as? String
		self.writerid = aDecoder.decodeObjectForKey(kBookWriteridKey) as? String
		self.load = aDecoder.decodeObjectForKey(kBookLoadKey) as? String
		self.content = aDecoder.decodeObjectForKey(kBookContentKey) as? String
		self.consideration = aDecoder.decodeObjectForKey(kBookConsiderationKey) as? String
		self.addDate = aDecoder.decodeObjectForKey(kBookAddDateKey) as? String
		self.image = aDecoder.decodeObjectForKey(kBookImageKey) as? String
		self.textDate = aDecoder.decodeObjectForKey(kBookTextDateKey) as? String
		self.htmlSize = aDecoder.decodeObjectForKey(kBookHtmlSizeKey) as? String
		self.keywords = aDecoder.decodeObjectForKey(kBookKeywordsKey) as? String
		self.read = aDecoder.decodeObjectForKey(kBookReadKey) as? String
		self.subject = aDecoder.decodeObjectForKey(kBookSubjectKey) as? String
		self.subject3 = aDecoder.decodeObjectForKey(kBookSubject3Key) as? String
		self.upDate = aDecoder.decodeObjectForKey(kBookUpDateKey) as? String

    }

    public func encodeWithCoder(aCoder: NSCoder) {
		aCoder.encodeObject(categories, forKey: kBookCategoriesKey)
		aCoder.encodeObject(active, forKey: kBookActiveKey)
		aCoder.encodeObject(parent, forKey: kBookParentKey)
		aCoder.encodeObject(isActive, forKey: kBookIsActiveKey)
		aCoder.encodeObject(pdfSize, forKey: kBookPdfSizeKey)
		aCoder.encodeObject(name, forKey: kBookNameKey)
		aCoder.encodeObject(lang, forKey: kBookLangKey)
		aCoder.encodeObject(flag, forKey: kBookFlagKey)
		aCoder.encodeObject(imageOriginal, forKey: kBookImageOriginalKey)
		aCoder.encodeObject(internalIdentifier, forKey: kBookInternalIdentifierKey)
		aCoder.encodeObject(transId, forKey: kBookTransIdKey)
		aCoder.encodeObject(minbody, forKey: kBookMinbodyKey)
		aCoder.encodeObject(pdfPureFile, forKey: kBookPdfPureFileKey)
		aCoder.encodeObject(pages, forKey: kBookPagesKey)
		aCoder.encodeObject(visit, forKey: kBookVisitKey)
		aCoder.encodeObject(subject2, forKey: kBookSubject2Key)
		aCoder.encodeObject(pdfFile, forKey: kBookPdfFileKey)
		aCoder.encodeObject(writerid, forKey: kBookWriteridKey)
		aCoder.encodeObject(load, forKey: kBookLoadKey)
		aCoder.encodeObject(content, forKey: kBookContentKey)
		aCoder.encodeObject(consideration, forKey: kBookConsiderationKey)
		aCoder.encodeObject(addDate, forKey: kBookAddDateKey)
		aCoder.encodeObject(image, forKey: kBookImageKey)
		aCoder.encodeObject(textDate, forKey: kBookTextDateKey)
		aCoder.encodeObject(htmlSize, forKey: kBookHtmlSizeKey)
		aCoder.encodeObject(keywords, forKey: kBookKeywordsKey)
		aCoder.encodeObject(read, forKey: kBookReadKey)
		aCoder.encodeObject(subject, forKey: kBookSubjectKey)
		aCoder.encodeObject(subject3, forKey: kBookSubject3Key)
		aCoder.encodeObject(upDate, forKey: kBookUpDateKey)

    }

}
