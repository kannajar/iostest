//
//  Category.swift
//
//  Created by khaledannajar on 3/3/16
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class Category: NSObject, NSCoding {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kCategorySuccessKey: String = "success"
	internal let kCategoryTimeKey: String = "time"
	internal let kCategoryBookKey: String = "books"


    // MARK: Properties
	public var success: Int?
	public var time: Time?
	public var books: [Book]?


    // MARK: SwiftyJSON Initalizers
    /**
    Initates the class based on the object
    - parameter object: The object of either Dictionary or Array kind that was passed.
    - returns: An initalized instance of the class.
    */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }

    /**
    Initates the class based on the JSON that was passed.
    - parameter json: JSON object from SwiftyJSON.
    - returns: An initalized instance of the class.
    */
    public init(json: JSON) {
		success = json[kCategorySuccessKey].int
		time = Time(json: json[kCategoryTimeKey])
		books = []
		if let items = json[kCategoryBookKey].array {
			for item in items {
				books?.append(Book(json: item))
			}
		} else {
			books = nil
		}

    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if success != nil {
			dictionary.updateValue(success!, forKey: kCategorySuccessKey)
		}
		if time != nil {
			dictionary.updateValue(time!.dictionaryRepresentation(), forKey: kCategoryTimeKey)
		}
		if books?.count > 0 {
			var temp: [AnyObject] = []
			for item in books! {
				temp.append(item.dictionaryRepresentation())
			}
			dictionary.updateValue(temp, forKey: kCategoryBookKey)
		}

        return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
		self.success = aDecoder.decodeObjectForKey(kCategorySuccessKey) as? Int
		self.time = aDecoder.decodeObjectForKey(kCategoryTimeKey) as? Time
		self.books = aDecoder.decodeObjectForKey(kCategoryBookKey) as? [Book]

    }

    public func encodeWithCoder(aCoder: NSCoder) {
		aCoder.encodeObject(success, forKey: kCategorySuccessKey)
		aCoder.encodeObject(time, forKey: kCategoryTimeKey)
		aCoder.encodeObject(books, forKey: kCategoryBookKey)

    }

}
